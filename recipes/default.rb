# Cookbook Name:: Gitlab-Nessus
# Recipe:: GitLab-Nessus
# License:: MIT
#
# Copyright 2019, GitLab Inc.

apt_repository 'gitlab-aptly' do
  uri 'http://aptly.gitlab.com/gitlab-utils'
  key 'http://aptly.gitlab.com/release.asc'
  distribution 'xenial'
  components ['main']
end

apt_package 'nessus' do
  version node['nessus']['version']
  options '--allow-unauthenticated'
  action :install
  not_if "dpkg -i nessus | grep -q #{node['nessus']['version']}"
  notifies :run, 'execute[restart nessusd]', :immediately
end

# for some reason we need to restart nessusd after package installation to make it work with systemd
execute 'restart nessusd' do
  command '/bin/systemctl restart nessusd'
  action :nothing
end

service 'nessusd' do
  action :enable
  action :start
end
