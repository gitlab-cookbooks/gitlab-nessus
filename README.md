# gitlab-nessus cookbook

This is a reimaginging of the [chef-nessus cookbook](https://supermarket.chef.io/cookbooks/nessus), tailored for the GitLab prod/stg/ops GCP environment and to integerate with Tenable.io's SaaS.

The intent is to:

1. Download the installer from a GitLab hosted (aptly/GCP bucket) location.
1. Install it to the target host.
1. Configure to meet our requirements (likely based off the chef-nessus cookbook)

With the intent to (at first) be manually registered with the Tenable.io SaaS, then automatically in the future.

Further information on the Tenable.io project is in the [Tenable Blueprint](https://docs.google.com/document/d/1mpgPDUNbu24iOHUtwgPxDJyTumOkeZqZ5VkvbbZL31M) and [deployment issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6318)

## Nessus Installer

Tenable owns Nessus bug continues to release the free/opensource version, downloadable here: [https://www.tenable.com/downloads/nessus](https://www.tenable.com/downloads/nessus)

