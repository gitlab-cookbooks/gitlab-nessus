# InSpec tests for recipe template::gitlab-nessus

control 'nessusd-installed' do
  impact 1.0
  title 'Verify nessusd installed successfully'
  desc '
      Confirms the Nessus package installed correctly by:
       - Checking if the package is recognized in dpkg
       - Confirm the existance of the nessusd binary
      '

  describe package('nessus') do
    it { should be_installed }
  end

  describe file('/opt/nessus/sbin/nessusd') do
    its('path') { should cmp '/opt/nessus/sbin/nessusd' }
    its('mode') { should cmp '0755' }
  end
end

control 'nessusd-active' do
  impact 1.0
  title 'Verify nessusd is active'
  desc 'Confirms nessusd is enabled and running'

  describe service('nessusd') do
    it { should be_enabled }
    it { should be_running }
  end
end
